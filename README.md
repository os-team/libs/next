# Next.js shared components

Shared components for Next.js applications.

- [@os-team/next-head](https://gitlab.com/os-team/libs/next/-/tree/mainackages/next-head) – Adds to the `head` tag meta tags (charset, viewport, etc), favicons, manifest.
- [@os-team/next-nprogress](https://gitlab.com/os-team/libs/next/-/tree/main/packages/next-nprogress) – Slim progress bar for Next.js applications.
