import reactConfig from 'eslint-config-os-team-react';

export default [
  ...reactConfig.configs.recommended,
  {
    ignores: ['**/dist/'],
  },
  {
    files: ['**/*.{js,mjs,cjs,ts,jsx,tsx}'],
    rules: {},
  },
];
