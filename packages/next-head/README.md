# @os-team/next-head [![NPM version](https://img.shields.io/npm/v/@os-team/next-head)](https://yarnpkg.com/package/@os-team/next-head) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/next-head)](https://bundlephobia.com/result?p=@os-team/next-head)

Adds to the head tag: meta tags (charset, viewport, title, description, theme-color), favicons, and manifest.

## Usage

### Step 1. Install the package

Install the package using the following command:

```
yarn add @os-team/next-head
```

### Step 2. Place favicons in the `static` folder

Required favicons:

| Path                       | Size    |
| -------------------------- | ------- |
| favicon-16x16.png          | 16x16   |
| favicon-32x32.png          | 32x32   |
| favicon.ico                | 48x48   |
| apple-touch-icon.png       | 180x180 |
| android-chrome-192x192.png | 192x192 |
| android-chrome-512x512.png | 512x512 |

You can generate favicons and manifest using [favicons.io](https://favicon.io).

### Step 3. Create the `manifest.json` file in the `static` folder

The manifest can contain the following content:

```json
{
  "name": "Project",
  "short_name": "Project",
  "icons": [
    {
      "src": "/android-chrome-192x192.png",
      "sizes": "192x192",
      "type": "image/png"
    },
    {
      "src": "/android-chrome-512x512.png",
      "sizes": "512x512",
      "type": "image/png"
    }
  ],
  "start_url": "/?utm_source=pwa",
  "display": "standalone",
  "theme_color": "#ffffff",
  "background_color": "#ffffff"
}
```

### Step 4. Place the tag in the page component

```tsx
<>
  <Head title='Title' description='Description' />
  <h1>Title</h1>
  <p>Lorem ipsum</p>
</>
```

See the [Next.js documentation](https://nextjs.org/docs/api-reference/next/head) for more information about the `head` component.
