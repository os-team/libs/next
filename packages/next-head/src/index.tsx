// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import NextHead from 'next/head.js';
import React from 'react';

interface HeadProps {
  title: string;
  description?: string;
  themeColor?: string;
  children?: React.ReactNode;
}

const Head: React.FC<HeadProps> = ({
  title,
  description,
  themeColor,
  children,
}) => (
  <NextHead>
    <meta charSet='UTF-8' />
    <meta
      name='viewport'
      content='width=device-width, initial-scale=1, viewport-fit=cover'
    />
    {themeColor && <meta name='theme-color' content={themeColor} />}
    {description && <meta name='description' content={description} />}
    <title>{title}</title>

    <link rel='apple-touch-icon' sizes='180x180' href='/apple-touch-icon.png' />
    <link rel='icon' type='image/png' sizes='32x32' href='/favicon-32x32.png' />
    <link rel='icon' type='image/png' sizes='16x16' href='/favicon-16x16.png' />
    <link rel='icon' type='image/x-icon' href='/favicon.ico' />
    <link rel='manifest' href='/manifest.json' />

    {children}
  </NextHead>
);

export default Head;
