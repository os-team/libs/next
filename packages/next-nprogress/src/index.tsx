import { css, Global } from '@emotion/react';
import { type SerializedStyles } from '@emotion/serialize';
import { clr, type Theme } from '@os-design/theming';
import { type NextRouter } from 'next/router.js';
import BaseNProgress from 'nprogress';
import React from 'react';

const startProgress = () => BaseNProgress.start();

const stopProgress = (timer: number) => {
  clearTimeout(timer);
  BaseNProgress.done();
};

export const initNProgress = (router: NextRouter, delayStartMs = 100): void => {
  const showProgressBar = () => {
    const timer = setTimeout(startProgress, delayStartMs);
    router.events.on('routeChangeComplete', () => stopProgress(timer));
    router.events.on('routeChangeError', () => stopProgress(timer));
  };
  router.events.on('routeChangeStart', showProgressBar);
};

const styles = (theme: Theme): SerializedStyles => css`
  #nprogress {
    pointer-events: none;
  }
  #nprogress .bar {
    background: ${clr(theme.colorPrimary)};
    position: fixed;
    z-index: 10001;
    top: 0;
    left: 0;
    width: 100%;
    height: 0.125em;
  }
  #nprogress .peg {
    display: block;
    position: absolute;
    right: 0;
    width: 6.25em;
    height: 100%;
    box-shadow:
      0 0 0.6em ${clr(theme.colorPrimary)},
      0 0 0.3em ${clr(theme.colorPrimary)};
    opacity: 1;
    transform: rotate(3deg) translate(0, -0.25em);
  }
`;

export const NProgressGlobalStyles: React.FC = () => <Global styles={styles} />;
