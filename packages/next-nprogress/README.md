# @os-team/next-nprogress [![NPM version](https://img.shields.io/npm/v/@os-team/next-nprogress)](https://yarnpkg.com/package/@os-team/next-nprogress) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/next-nprogress)](https://bundlephobia.com/result?p=@os-team/next-nprogress)

Slim progress bar for Next.js applications.

Initializes [nprogress](https://github.com/rstacruz/nprogress) for Next.js and applies styles using [Emotion](https://emotion.sh).

## Usage

### Step 1. Install the package

```
yarn add @os-team/next-nprogress
```

### Step 2. Initialize nprogress in the `./pages/_app.tsx` file

```tsx
import React, { useEffect, useMemo } from 'react';
import { initNProgress, NProgressGlobalStyles } from '@os-team/next-nprogress';
import { ThemeProvider } from '@os-design/theming';
import { useRouter } from 'next/router';

const App: React.FC = ({ Component, pageProps }) => {
  const router = useRouter();

  // Initialize nprogress
  useEffect(() => {
    initNProgress(router);
  }, [router]);

  return (
    <ThemeProvider>
      <NProgressGlobalStyles />
      <Component {...pageProps} />
    </ThemeProvider>
  );
};

export default App;
```

Note that `NProgressGlobalStyles` must be inside `ThemeProvider`, as it uses the colorPrimary from theme.
